Name:           os-prober
Version:        1.81
Release:        1
Summary:        Probe disks on the system for other operating systems
License:        GPLv2+ and GPL+
URL:            http://kitenet.net/~joey/code/os-prober/
Source0:        https://salsa.debian.org/installer-team/os-prober/-/archive/%{version}/%{name}-%{version}.tar.gz

Patch0:         os-prober-mdraidfix.patch
Patch1:         os-prober-bootpart-name-fix.patch
Patch2:         os-prober-mounted-partitions-fix.patch
Patch3:         os-prober-umount-fix.patch
Patch4:         os-prober-grub2-parsefix.patch
Patch5:         os-prober-grepfix.patch

BuildRequires:  gcc 
Requires:       udev coreutils util-linux grep /bin/sed /sbin/modprobe grub2-tools-minimal

%description
Os-prober can probe disks on the system for other operating systems,
and add them to the boot loader, so that installing current OS doesn't
make your other installed OS hard to boot.

%prep
%autosetup -n %{name}-%{version} -p1
find -type f -exec sed -i -e 's|usr/lib|usr/libexec|g' {} \;
sed -i -e 's|grub-probe|grub2-probe|g' os-probes/common/50mounted-tests \
     linux-boot-probes/common/50mounted-tests
sed -i -e 's|grub-mount|grub2-mount|g' os-probes/common/50mounted-tests \
     linux-boot-probes/common/50mounted-tests common.sh

%build
%make_build

%install
install -m 0755 -d $RPM_BUILD_ROOT%{_bindir}
install -m 0755 -d $RPM_BUILD_ROOT%{_var}/lib/%{name}

install -m 0755 -p os-prober linux-boot-prober $RPM_BUILD_ROOT%{_bindir}
install -m 0755 -Dp newns $RPM_BUILD_ROOT%{_libexecdir}/os-prober/newns
install -m 0644 -Dp common.sh $RPM_BUILD_ROOT%{_datadir}/%{name}/common.sh

%ifarch m68k
ARCH=m68k
%endif
%ifarch ppc ppc64
ARCH=powerpc
%endif
%ifarch sparc sparc64
ARCH=sparc
%endif
%ifarch %{ix86} x86_64
ARCH=x86
%endif

for probes in os-probes os-probes/mounted os-probes/init \
              linux-boot-probes linux-boot-probes/mounted; do
        install -m 755 -d $RPM_BUILD_ROOT%{_libexecdir}/$probes 
        cp -a $probes/common/* $RPM_BUILD_ROOT%{_libexecdir}/$probes
        if [ -e "$probes/$ARCH" ]; then 
                cp -a $probes/$ARCH/* $RPM_BUILD_ROOT%{_libexecdir}/$probes 
        fi
done
if [ "$ARCH" = x86 ]; then
        install -m 755 -p os-probes/mounted/powerpc/20macosx \
            $RPM_BUILD_ROOT%{_libexecdir}/os-probes/mounted
fi

%pre

%preun

%post

%postun

%files
%doc README TODO debian/changelog
%license debian/copyright
%{_bindir}/*
%{_datadir}/%{name}
%{_var}/lib/%{name}
%{_libexecdir}/*

%changelog
* Sat Jan 28 2023 yixiangzhike <yixiangzhike007@163.com> - 1.81-1
- update version to 1.81

* Tue Oct 25 2022 yanglongkang <yanglongkang@h-partners.com> - 1.79-2
- rebuild for next release

* Wed Dec 29 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 1.79-1
- update version to 1.79

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.77-3
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Wed May 26 2021 yangzhuangzhuang<yangzhuangzhuang1@huawei.com> - 1.77-2
- Remove code using device mapper

* Thu Jul 23 2020 linwei<linwei54@huawei.com> - 1.77-1
- update os-prober to 1.77

* Thu Feb 27 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.74-12
- fix os-prober command failed.

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.74-11
- Delete useless files.

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.74-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimize spec file.

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 1.74-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix spec rule in openeuler

* Thu Sep 05 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.74-8
- Package init
